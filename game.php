<?php
/**
 * Template Name: Game pagina
 *
 * @package wegwijsdag
 */

get_header(); ?>
	<div class="contentTop">
		<div class="subMenu">
			<?php get_sidebar('Submenu'); ?>
		</div>
	</div>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
		</main>
		<!-- #main -->
	</div><!-- #primary -->
	<div class="contentSection">
		<div class="clear">
			<center>
	  			<div id="gameContainer" class="webgl-content" style="width: 960px; height: 540px;"></div>
  			
  			<a class="btn arrow-right big green" href="<?php echo esc_url( home_url( '/' ) ); ?>workshops-aanmelden">Uitgespeeld? Kies je workshops</a>
  			</center>
    	</div>

		<link rel="stylesheet" href="<?php echo esc_url( home_url( '/' ) ); ?>game/<?php echo ORYENTE_VERSION;?>/TemplateData/style.css">
		<script src="<?php echo esc_url( home_url( '/' ) ); ?>game/<?php echo ORYENTE_VERSION;?>/TemplateData/UnityProgress.js"></script>
		<script src="<?php echo esc_url( home_url( '/' ) ); ?>game/<?php echo ORYENTE_VERSION;?>/Build/UnityLoader.js"></script>
		<script>
      		var gameInstance = UnityLoader.instantiate("gameContainer", "<?php echo esc_url( home_url( '/' ) ); ?>game/<?php echo ORYENTE_VERSION;?>/Build/<?php echo ORYENTE_VERSION;?>.json", {onProgress: UnityProgress});
	  	</script>

		<br/>

		<div class="contentItems">
			<div class="item orange">
				<div class="itemArrow">
					<div class="arrow"></div>
				</div>
				<div class="itemContent">
					<h3>Welke browser moet ik gebruiken?</h3>
					<p>Gebruik voor de beste werking één van de browsers hieronder:<br/><br/>
					Chrome, Firefox, Edge laatste versie<br/>
					Windows: 8.1 of hoger</p>
				</div>
			</div>
			<div class="item green">
				<div class="itemArrow">
					<div class="arrow"></div>
				</div>
				<div class="itemContent">
					<h3>Laadt de game niet op de site?</h3>
					<p>De game werkt op zijn best in Chrome of Firefox. Werkt het dan nog steeds niet, download dan de app via de App-store of Google Play zodat je de game op je telefoon kunt spelen.</p>
				</div>
			</div>
		</div>
		
	</div>
<?php
get_footer();
