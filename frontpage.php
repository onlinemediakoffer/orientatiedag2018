<?php
/**
 * Template Name: Voorpagina
 *
 * @package wegwijsdag
 */

get_header(); ?>
	<div class="contentTop">
		<div class="subMenu">
			<?php get_sidebar('Submenu'); ?>
		</div>
	</div>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="homepageHeaderButtons">
				<a class="btn-bigButton" href="<?php echo esc_url( home_url( '/' ) ); ?>speel-de-game"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/speel-de-game.png"></a><br/>
				<a class="btn-ghost" href="<?php echo esc_url( home_url( '/' ) ); ?>workshops-aanmelden">Game uitgespeeld? Klik hier</a>
			</div>
			<div class="homeHeaderPeople"></div>
			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
		</main><!-- #main -->
	</div><!-- #primary -->
</div>
<div class="homepageContent">
	<div class="homepageStep">
		<div class="site-content">
			<div class="right">
				<h2><span class="trans">Stap 1</span><br/><strong>Speel de game</strong></h2>
				<p>Om te ontdekken waar jij goed in bent en wat je leuk vindt, hebben we een game gemaakt: Oryente. Je kunt meteen aan de slag!</p><br/>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>speel-de-game" class="btn arrow-right blue">Begin direct</a>
				<div class="HpArts"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/arts.png"></div>
			</div>
			<div class="left">
				<h2><span class="trans">Stap 2</span><br/><strong>Kies 3 workshops</strong></h2>
				<p>Op basis van jouw high scores kies je drie workshops die bij je passen. Daarna meld je je eenvoudig aan voor de Oriëntatiedag.</p><br/>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>workshops-aanmelden" class="btn arrow-right yellow">Kies je workshops</a>
				<div class="HpBankier"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/bankier.png"></div>
			</div>
			<div class="right">
				<h2><span class="trans">Stap 3</span><br/><strong>DE ORIËNTATIEDAG</strong></h2>
				<p>Kom op woensdag 23 mei naar de Oriëntatiedag, volg de workshops en ontdek welke studies wel of juist niet bij jou passen.</p><br/>
				<div class="HpArtiest"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/artiest.png"></div>
			</div>
		</div>
	</div>
	<!-- <div class="contentSection">
		<div class="site-content">
			<h2>Welk profiel kies jij?</h2>
			<p>Na de Wegwijsdag heb jij hopelijk een goed idee welk profiel bij jou past.<Br/> Hieronder zetten we ze graag voor je op een rijtje.</p>
			<div class="profileblock">
				<ul>
					<li><a class="blue" href="<?php echo esc_url( site_url( '/' ) ); ?>profielen/cultuur-maatschappij">Cultuur &<br/> Maatschappij</a></li>
					<li><a class="yellow" href="<?php echo esc_url( site_url( '/' ) ); ?>profielen/economie-maatschappij">Economie &<br/> Maatschappij</a></li>
					<li><a class="green" href="<?php echo esc_url( site_url( '/' ) ); ?>profielen/natuur-gezondheid">Natuur &<br/> Gezondheid</a></li>
					<li><a class="pink" href="<?php echo esc_url( site_url( '/' ) ); ?>profielen/natuur-techniek">Natuur &<br/> Techniek</a></li>
				</ul>
			</div>
		</div>
	</div> -->

<?php
get_footer();
