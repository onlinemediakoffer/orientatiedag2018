<?php
/**
 * Template Name: Contactpagina
 *
 * @package wegwijsdag
 */

get_header(); ?>
	<div class="contentTop">
		<div class="subMenu">
			<?php get_sidebar('Submenu'); ?>
		</div>
	</div>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
		</main><!-- #main -->
	</div><!-- #primary -->
	<div class="contentSection">
		<div class="titleItemWide">
			<h2>Problemen met de game?<br/> Hieronder staan een paar tips.
			</h2>
		</div>
		<div class="contentItems">
			<div class="item red">
				<div class="itemArrow">
					<div class="arrow"></div>
				</div>
				<div class="itemContent">
					<h3>De game op je smartphone</h3>
					<p>Let erop dat je tussentijds niet kunt wisselen tussen devices. Je moet hem helemaal uitspelen op je telefoon. Maar natuurlijk kun je tussendoor wel stoppen. Met de inlogcode die je dan ontvangt via de mail kun je later gewoon weer inloggen en gaat de game verder waar je gebleven bent.</p>
				</div>
			</div>
			<div class="item orange">
				<div class="itemArrow">
					<div class="arrow"></div>
				</div>
				<div class="itemContent">
					<h3>Laadt de game niet op de site?</h3>
					<p>De game werkt op zijn best in Chrome of Firefox. Werkt het dan nog steeds niet, download dan de app voor via de App-store of Google Play zodat je de game op je telefoon kunt spelen.</p>
				</div>
			</div>
		</div>
		<div class="titleItemWide">
			<h2>Stel jouw vraag</h2>
			<?php  echo do_shortcode( '[gravityform id="2" title="false" description="false" ajax="true"]' ); ?>
		</div>
	</div>
<?php
get_footer();
