<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wegwijsdag
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<meta name="apple-itunes-app" content="app-id=1088201318">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,900" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,400i,900,900i" rel="stylesheet">
	<link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.png" type="image/x-icon" />
	<link rel="mask-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/safari-pinned-tab.svg" color="#a7c949">
	<link rel="apple-touch-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/apple-touch-icon.png">
	<?php if(is_page(43)){gravity_form_enqueue_scripts(1, true);}?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'wegwijsdag' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="site-branding">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/oryente-logo.png" /></a>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation">
			<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				) );
			?>
		</nav>
		<div class="playButtonHeader">
			<a class="playButtonHeaderHyperlink" href="<?php echo esc_url( home_url( '/' ) ); ?>speel-de-game">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/speel-de-game.png">
			</a>
			<a class="shiftnav-toggle shiftnav-toggle-button" data-shiftnav-target="shiftnav-main"><i class="fa fa-bars"></i> Menu </a>
		</div><!-- #site-navigation -->
	</header><!-- #masthead -->
	<div class="contentTop">
		<div class="subMenu">
			<?php get_sidebar('Submenu'); ?>
		</div>
	</div>
	<div id="content" class="site-content">
