<?php
/**
 * Template Name: Orientatiedag
 *
 * @package wegwijsdag
 */

get_header(); ?>
	<div id="primary" class="content-area">
		<div class="HpBankier"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/arts.png"></div>
		<div class="HpBankier"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/artiest.png"></div>
		<main id="main" class="site-main" role="main">
			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
		</main><!-- #main -->
		<div class="contentSection">
			<div class="row">
				<div class="quote">
					<div class="quoteContent">
						<h3>Case: Schets het ideale dierenverblijf</h3><br/>
						‘De case-opdracht was: ontwerp een leefomgeving voor een ijsbeer, paard, kip, kat of koe, rekening houdend met dierwelzijn, de kosten, locatie en groepsindeling. Een van de ontwerpen was heel origineel: een boomhut voor katten. De case was een voorproefje van de studie Diermanagement, die ik zelf doe.  De leerlingen waren enthousiast en stelden veel vragen.’<br/><Br/>
						<span>Fleur Boele<br/>
						Case-docent en derdejaarsstudent<br/>
					</div>
				</div>
				<div class="todo">
					<h2>WAT KUN JE VERWACHTEN OP DE ORIËNTATIEDAG?</h2>
					<p>
					Ben jij graag lekker actief bezig of houd je ervan over dingen na te denken? Heb je groene vingers, een hoofd vol creatieve ideeën of vind je techniek ontzettend leuk? Tijdens de Oriëntatiedag ontdek je waar jouw talenten liggen.</p>
					<h3>Ontdek door te doen! </h3>
					<p>Stil zitten en luisteren naar een docent die jou vertelt wat je moet kiezen? Nee hoor! Jij gaat zelf aan de slag tijdens twee workshops. Zo kun je meteen kennismaken met de studies waar je uit kunt kiezen. </p>
				</div>
			</div>
			<div class="row second">
				<div class="watDoen">
					<h2>Wat moet je doen?</h2><br/>
					<p>Nadat je de game hebt gespeeld, vul je jouw high scores in. Wij mixen en matchen je met de juiste drie workshops. Daarna kun je je met één klik op de knop aanmelden voor de Oriëntatiedag.</p>
				</div>
				<div class="speelDeGame">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>speel-de-game"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/playButtonSmal.png" /></a>
				</div>
			</div>
			<div class="row third">
				<div class="stepOne">
					<h5 class="green">Stap 1</h5>
					<p>Play the game en ontdek waar jij goed in bent en wat je leuk vindt!</p>
				</div>
				<div class="stepTwo">
					<h5 class="pink">Stap 2</h5>
					<p>Vul je high scores in en vind jouw workshop match.<br/>
					Meld je daarna makkelijk aan voor de Oriëntatiedag.</p>
				</div>
				<div class="stepThree">
					<h5 class="blue">Stap 3</h5>
					<p>Kom naar de Oriëntatiedag, volg je workshops en ontdek welke studies bij je passen.</p>
				</div>
			</div>
			<div class="row four">
				<div class="intro">
					<h2>Sfeerimpressies 2017</h2>
					<p>Vorig jaar organiseerden we ook een Oriëntatiedag. Maar liefst 200 havo-leerlingen volgden leuke workshops en kregen een voorproefje van de studies die mogelijk zijn bij NHL Stenden Hogeschool en Van Hall Larenstein. Hieronder vind je een aantal foto's van deze dag om alvast een goed beeld te krijgen van wat jou te wachten staat.</p>
				</div>
				<div class="rigthImage">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/bankier.png" />
				</div>
				<div class="fotoBlok">
					<div class="blockOne"></div>
					<div class="blockTwo"></div>
					<div class="blockThree"></div>
					<div class="blockFour"></div>
					<div class="blockFive"></div>
					<div class="blockSix"></div>
				</div>
				</div>
			</div>
		</div>
	</div><!-- #primary -->
	
<?php
get_footer();
