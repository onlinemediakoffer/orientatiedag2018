/**
 * Created by Richard on 18-10-17.
 */

// JS fiddle http://jsfiddle.net/tuu0wdhb/
// HTML: <input class="single-checkbox"type="checkbox" name="test" value="1" />Item 1
jQuery( document ).ready( function( $ ) {


    // $() will work as an alias for jQuery() inside of this function
    //[ your code goes here ]
    $('.workshop-checkbox').on('change', function() {
        if($('.workshop-checkbox:checked').length > 3) {
            this.checked = false;
           // alert("Reeds twee workshops gekozen, deselecteer eerst 1 workshop");
        }else{
            $('#workshops .mix > .workshop-checkbox:not(:checked)+label').fadeIn(250).removeClass('inactive');
            // knop om door te gaan
            $('#workshop_submitbar').fadeIn(250).removeClass('visible');
        }




        if($('.workshop-checkbox:checked').length == 3) {
            $('#workshops .mix > .workshop-checkbox:not(:checked)+label').fadeIn(250).addClass('inactive');

            // knop om door te gaan
            $('#workshop_submitbar').fadeIn(250).addClass('visible');
           // $(this).addClass('selected');
        }


    });

    if($('.workshop-checkbox:checked').length < 3) {
        $('input:checkbox').change(function () {
            if ($(this).is(":checked")) {
             $(this).addClass("selected");
            }
            else{
                $(this).removeClass("selected");
            }
        });
    }

    $('.workshop-checkbox:checked').on('change', function() {
       if($('.workshop-checkbox:checked').length != 3) {
            $('#workshops .mix > .workshop-checkbox:not(:checked)+label').fadeIn(250).removeClass('inactive');

           // knop om door te gaan
           $('#workshop_submitbar').fadeIn(250).removeClass('visible');

       }
        // if($('.workshop-checkbox:checked').length < 2) {
        //     $('#workshops > .workshop-checkbox:not(:checked)+label').fadeIn(250).toggleClass('inactive');
        // }
    });


    // // OUD, gebruiken nu de mixitup library
    // // Toggle workshops
    // var $btns = $('.check').click(function() {
    //     if (this.id == 'all') {
    //         $('#workshops > label').fadeIn(250);
    //         $('#workshops > input').fadeIn(250);
    //
    //     } else {
    //         var $el = $('.' + this.id);
    //         // $('#workshops > label').not($el).hide();
    //         // $('#workshops > input').not($el).hide();
    //
    //         $('#workshops > input').not($el).toggleClass('show');
    //         $('#workshops > label').not($el).toggleClass('show');
    //         $('.' + this.id).fadeIn(250).addClass('show');
    //     }
    //     // $btns.toggleClass('active');
    //     $(this).toggleClass('active');
    // })


     //End jQuery wrapper
} );

var containerEl = document.querySelector('.container');

var mixer = mixitup(containerEl, {
    multifilter: {
        enable: true
    },
    animation: {
        enable: true,
        effects: 'fade scale',
        applyPerspective: true,
        animateResizeTargets: true,
        nudge: true,
        animateResizeContainer: true,
        // clampWidth: false,
        clampHeight: true
    }
});
