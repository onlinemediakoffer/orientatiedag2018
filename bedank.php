<?php
/**
 * Template Name: Bedankpagina
 *
 * @package wegwijsdag
 */

get_header(); ?>
	<div class="contentTop">
		<div class="subMenu">
			<?php get_sidebar('Submenu'); ?>
		</div>
	</div>
	<div id="primary" class="content-area">
		<div class="contentSection">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/bedankPeople.png" />
		</div>
		<main id="main" class="site-main" role="main">
			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
		</main><!-- #main -->
	</div><!-- #primary -->
	
<?php
get_footer();
